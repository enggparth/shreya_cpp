/***************************************************************************
 *
 * Program to access global and userdefined namespace in local body.
 *
 **************************************************************************/

/**********************
 * INCLUDE
 *********************/

#include <iostream>

/*******************
 * NAMESPACE 
 *******************/

#include "1.h"
using namespace std;//global namespace

namespace first // userdefined namespace
{
	int val = 500;
}
int val = 100; 
int main()
{
	int val = 50;
	cout << val; // access local variable
	cout << first :: val; // access variable of first-namespace
	cout<< :: val<<"\n"; // access global variable
}
