/******************************************************************
 *
 * experiment string str functions with different printing method
 *
 ******************************************************************

/**********************
 * INCLUDE
 *********************/

#include "10.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;
int main()
{
	string str1("hello World");
        string str2(str1);
	string str3( 5, '#' );   
    	string str4(str1, 6, 6);
	string str5(str2.begin(), str2.begin() + 5);
	cout << str1 << endl << str2 << endl << str3 << endl << str4 << endl << str5 << endl; 
}
