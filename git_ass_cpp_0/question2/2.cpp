/************************************************************************
 *
 * access namespace variables inside other namespace and observe answers
 *
 ************************************************************************/

/**********************
 * INCLUDE
 *********************/

#include "2.h"

/*******************
 * NAMESPACE 
 *******************/

namespace Name1 
{
	int num1 = 1;
};
namespace Name2 
{
	int num1;
	int num2 = 2;
	namespace Name3 // namespace inside other namespace
	{
		using namespace Name1; // invoking namespace1
		int num2 = 3;
		int num3 = 4;
		int num4 = num1;
	};
};
using namespace Name2;
using namespace std;
int main()
{
	cout << Name2 :: num1 << endl;
	cout << Name2 :: num2 << endl;
	cout << Name3 :: num3 << endl;
	cout << Name3 :: num4 << endl;
	cout << Name1 :: num1 << endl;

}
