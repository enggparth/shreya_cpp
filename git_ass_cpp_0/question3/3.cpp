/**********************************************************************
 *
 * check how pointer works in cpp 
 *
 *********************************************************************/

/**********************
 * INCLUDE
 *********************/

#include "3.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;
int main()
{
	void *vp;
	char chr = 'A';
	int inti = 1;  
	float flt = 1.1;
	
	vp = &chr;
	
	int &ref = inti;
	float *ptr = &flt;

	cout << chr << " " << vp <<  " " << vp << endl;
	cout << inti << " " << ref << " " << &inti << endl;
	cout << flt << " " << *ptr << " " << &flt << endl;
	return 0;
}
