/*****************************************************************
 *
 * acessing string with pointer
 *
 ****************************************************************/

/**********************
 * INCLUDE
 *********************/

#include "4.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;
int main()
{
	string str;
	cout << "Enter a string ";
	cin >> str;
	cout << "String entered " << str << endl << " String address " << &str << endl;

	string* ptr = &str;
	cout << "acessing string address with pointer " << ptr << endl << "String value with pointer " << *ptr << endl;
}
