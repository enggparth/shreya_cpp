/*****************************************************************************************
 *
 * program to experiment reference call to variables
 *
 ****************************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "5.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;
int main()
{
	int num = 3;
	int &ref = num;

	cout << num << endl;
	cout << ref << endl;

	ref = 4;
	cout << num << endl;
        cout << ref << endl;
	
	num = 5;
	cout << num << endl;
        cout << ref << endl;

	/* addresses */
	cout << &num << endl;
	cout << &ref << endl;
	return 0;
}
