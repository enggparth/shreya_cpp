/*************************************************************************************
 *
 * find number of alphabets and decimals in the given string using isdigit and isalpha
 *
 *************************************************************************************/

/**********************
 * INCLUDE
 *********************/

#include "7.h"

/*******************
 * NAMESPACE 
 *******************/
using namespace std;

int main()
{
    string str;

    cout << "Enter a string ";
    cin >> str;
  
    int alpha = 0, num = 0, loop; 
    for (loop = 0; str[loop]!= '\0'; loop++) 
    { 
        /* check for alphabets */
        if (isalpha(str[loop]) != 0) 
            alpha++; 
  
        /* check for decimal digits */
        else if (isdigit(str[loop]) != 0) 
            num++; 
    } 
  
    cout <<"Alphabet letters" << alpha <<" Decimal numbers " << num << endl; 
    return 0; 
}
