/********************************************************************
 *
 * swap values using reference function
 *
 *******************************************************************/

/**********************
 * INCLUDE
 *********************/

#include "8.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

void swap(int &num1, int &num2)
{
	int swp;
	swp = num2;
	num2 = num1;
	num1 = swp;
}
int main()
{
	char num = '\0';
	char &ref = num;
	cout << ref << &ref << endl; 

	int num1, num2;
	string str;
	cout << "Enter value of 1 num ";
	cin >> str;
	int len = str.length();
        for(int loop = 0; loop < len; loop++)
        {
                if(isdigit(str[loop]) == 0)
                {
                        cout << "Enter valid integer" << endl;
                        exit(0);
                }
        }
        num1 = stoi(str);

	cout << "Enter value of 2 num ";
        cin >> str;
        len = str.length();
        for(int loop = 0; loop < len; loop++)
        {
                if(isdigit(str[loop]) == 0)
                {
                        cout << "Enter valid integer" << endl;
                        exit(0);
                }
        }
        num2 = stoi(str);


	cout <<"num1 = " << num1 << endl << "num2 = "<< num2 << endl;
	swap(num1, num2);
	cout <<"num1 = " << num1 << endl << "num2 = "<< num2 << endl;
}
