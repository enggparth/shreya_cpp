/**************************************************************
 *
 * accept a string and reverse it
 *
 *************************************************************/

/**********************
 * INCLUDE
 *********************/

#include "9.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;
int main()
{
	string str;
	cout << "Enter a string to reverse ";
	cin >> str;
	
	int len = str.length();
	char ch;
	
	for(int loop = 0; loop < len; loop++)
	{
		cout << str[(len - loop) - 1];
	}
	cout << endl;
	return 0;
}
