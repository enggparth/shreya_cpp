/**************************************************************************************
 *
 * Write a program and input two integers in class and pass them to default constructor
 * of the class. Show the result of the addition of two numbers.
 *
**************************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "1.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

/* creating class Add */
class Add
{
	/* keeping x, y and sum as public members so that it can be acessed from outside this scope */
	public:
		int num1, num2, sum;
                Add();
};
Add :: Add()
{
	{
		string str;
		int value;
		cout << "Enter value of 1st number";
		cin >> str;
		
		int valid = validate_int(str, value );

	        if(valid == 1)
	        {
        	        cout << "Enter valid numbers" << endl;
               		exit(0);
	        }
	        else
	        {
	                num1 = value;
	        }

		cout << "Enter value of 2nd number";
                cin >> num2;
	
		valid = validate_int(str, value );

                if(valid == 1)
                {
                        cout << "Enter valid numbers" << endl;
                        exit(0);
                }
                else
                {
                        num2 = value;
                }

		sum = num1 + num2;
	}
}


int main()
{
	Add ad; // Defaut constructors are called itself when the object is created.
	
	cout << "Sum of " << ad.num1 << " and " << ad.num2 << " is " << ad.sum << "\n" ;	
	return 0;
}
