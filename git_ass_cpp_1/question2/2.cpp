/************************************************************************
 *
 * Write a program which perform arithmetic operations. All the function
 * definitions should define outside the class.
 * 
 ************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "2.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

/* creating class od Artihmatic Operations */
class ArithmaticOperation
{
	/* keeping num1 and num2 as public members so that it can be acessed from outside this scope */
	public:
		float num1, num2;
		float getValues();
		void printValues();
		float addtion();
		float subtraction();
		float multiplication();
		float division();
};

/* to add numbers */

float ArithmaticOperation :: addtion()
{
	float sum = num1 + num2;
	return sum;
}

/* to subract numbers */

float ArithmaticOperation :: subtraction()
{
        float sub = num1 - num2;
        return sub;
}

/* to multiply numbers */

float ArithmaticOperation :: multiplication()
{
        float mul = num1 * num2;
        return mul;
}

/* to divide numbers */

float ArithmaticOperation :: division()
{ 	
        float div = num1 / num2;
        return div;
}

/* to get values. We use Rerferences as arguments here */

float ArithmaticOperation :: getValues()
{
	float value;
	string str1, str2;
	cout << "Enter 2 numbers to perform all artimatic operations on it. ";
        cin >> str1;
	cin >> str2;

	/* take input in string for validation */
        int valid = validate_float(str1, value );

        if(valid == 1)
        {
		cout << "Enter valid numbers";
                exit(0);
        }
	else
	{
		num1 = value;
	}
	valid = validate_float(str2, value);
	if(valid == 1)
        {
		cout << "Enter valid numbers";
                exit(0);
        }
	else
        {
                num2 = value;
        }
}

/* to get values. We use Rerferences and Class object as arguments here */

void ArithmaticOperation :: printValues()
{
        cout << "addtion of "<< num1 << " and " << num2 << " is " << addtion() << endl;
        cout << "subtraction of "<< num1 << " and " << num2 << " is " << subtraction() << endl;
        cout << "multiplication of "<< num1 << " and " << num2 << " is " << multiplication() << endl;

        if( num2 == 0)
        {
                cout << "Denominator cannot be zero.\n";
                exit(0);
        }
        cout << "divison of "<< num1 << " and " << num2 << " is " << division() << endl;

}


int main()
{
	ArithmaticOperation opobj; // creating object of Arithmantic operations
	
	opobj.getValues();

	opobj.printValues();

	return 0;
}
