/**************************************************************************************
 *
 * Write a program which has Shape class. This class will overload the area() method to
 * find area of square (if area() has 1 argument), rectangle (if area() has 2 arguments),
 * trapezoid (if area() has 3 arguments).
*
**************************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "3.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

/* creating class Shape */
class Shape
{
	public:
		float area(float side); // for area of square
		float area(float side1, float side2); //for are of rectngle
		float area(float side1, float side2, float heigth); // for area of trapezoid
};

/* find area of square */

float Shape :: area(float side)
{
	float area = side * side;
	return area;
}

/* find area of rectangle */

float Shape :: area(float side1, float side2)
{
	float area = side1 * side2;
	return area;
}

/* find area of trapezoid */

float Shape :: area(float side1, float side2, float heigth)
{
	float area = ((side1 + side2) * heigth) / 2;
	return area;
}


int main()
{
	Shape shobj; // creating object of Shape class 
	float side1, side2, heigth;
	string str1, str2, str3;
	float value;

	cout << "Enter the side of square ";
	cin >> str1;
	
	/*validation */
	int valid = validate_float(str1, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                side1 = value;
        }
		
	cout << shobj.area(side1) << endl;
	
	cout << "Enter the 2 sides of rectangle ";
        cin >> str1 >> str2;
	
	valid = validate_float(str1, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                side1 = value;
        }
	valid = validate_float(str2, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                side2 = value;
        }

        cout << shobj.area(side1, side2) << endl;

	cout << "Enter 2 side and heigth of trapezoid ";
        cin >> side1 >> side2 >> heigth;
	
	valid = validate_float(str1, value );
	if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                side1 = value;
        }
        valid = validate_float(str2, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else 
        {
                side2 = value;
        }

	valid = validate_float(str3, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                heigth = value;
        }

        cout << shobj.area(side1, side2, heigth) << endl;

	return 0;
}
