/*****************************************************************************
 *
 * Define a class student with the following specification
 *
 * Private members of class student
 * admno integer
 * sname 20 character
 * eng. math, science float
 * total float
 * ctotal() a function to calculate eng + math + science with float return type.
 *
 * Public member function of class student
 * Takedata() Function to accept values for admno, sname, eng, science and invoke 
 * ctotal() to calculate total.
 * Showdata() Function to display all the data members on the screen.
 *
*******************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "4.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

/* creating Student class */
class Student
{
	/* declaring private members and member functions */
	private:
		int admno;
		string sname;
		float eng, math, science;
		float total;
		float ctotal();
	public:
	/* declaring public member functions */
		void takeData();
		void showData();
};

/* function to take dataform the user */

void Student :: takeData()
{
	int value_int;
	float value_ft;
	string str;
	cout << "Enter administration no. ";
	cin >> str;

	
	/* validation */
	int valid = validate_int(str, value_int );

        if(valid == 1)
        {
                cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                admno = value_int;
        }

	cout << "Enter student name ";
	cin >> sname;

	int loop = 0;
	while(sname[loop] != '\0')
	{
		if((sname[loop] >= 'a' && sname[loop] <= 'z') || (sname[loop] >= 'A' && sname[loop] <= 'Z'))
		{
			
		}
		else
		{
			cout << "Enter alphabets only\n";
			exit(0);
		}
		loop++ ;
	}
	cout << "Enter english marks ";
	cin >> str;

	valid = validate_float(str, value_ft);
        if(valid == 1)
        {
		cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                eng = value_ft;
        }

	cout << "Enter maths marks ";
        cin >> str;
	
	valid = validate_float(str, value_ft);
        if(valid == 1)
        {
		cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                math = value_ft;
        }

	cout << "Enter science marks ";
        cin >> str;
	
	valid = validate_float(str, value_ft);
	if(valid == 1)
        {
		cout << "Enter valid numbers" << endl;
                exit(0);
        }
        else
        {
                science = value_ft;
        }

	ctotal();
}

/* function to calculate total of marks */

float Student :: ctotal()
{
	total = eng + math + science;
}

/* function to display data */

void Student :: showData()
{
	cout << "Administration no: \t" << admno << endl;
	cout << "Student name: \t\t" << sname << endl;
	cout << "English Marks: \t\t" << eng << endl;
	cout << "Math Marks: \t\t" << math << endl;
	cout << "Science Marks: \t\t" << science << endl;
	cout << "Total Marks: \t\t" << total << endl;
}


int main()
{
	Student stuobj; // creating object of Student class
	
	stuobj.takeData();
	
	stuobj.showData();
	return 0;
}
