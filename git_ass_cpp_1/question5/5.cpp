/***************************************************************************************
 * 
 * Write the definition for a class called Rectangle that has floating point data members
 * length and width. The class has the following member functions:
 *
 * void setlength(float) to set the length data member
 * void setwidth(float) to set the width data member
 * float perimeter() to calculate and return the perimeter of the rectangle
 * float area() to calculate and return the area of the rectangle
 * void show() to display the length and width of the rectangle
 * int sameArea(Rectangle) that has one parameter of type Rectangle. sameAreareturns 1 if
 *the two Rectangles have the same area, and returns 0 if they don't.
 * 
 * 1.Write the definitions for each of the above member functions.
 * 
 * 2.Write main function to create two rectangle objects. Set the length and width of the 
 * first rectangle to 5 and 2.5. Set the length and width of the second rectangle to 5 and
 * 18.9. Display each rectangle and its area and perimeter.
 * 
 * 3.Check whether the two Rectangles have the same area and print a message indicating the
 * result. Set the length and width of the first rectangle to 15 and 6.3. Display each
 * Rectangle and its area and perimeter again. Again, check whether the twoRectangles have the
 *same area and print a message indicating the result.
 *
 *********************************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "5.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

/* creating class Rectangle */
class Rectangle
{
	public:
		float length, width, rec_area, rec_perimeter; // private members
		/* public member funcitons */
		void setLength(float);
		void setWidth(float);
		void objFunctions();
		float area();
		float perimeter();
		void show();
		int sameArea(Rectangle);
};

/* function to set length of rectangle */
void Rectangle :: setLength(float len)
{
	length = len;
}

/* function to set width of rectangle */
void Rectangle :: setWidth(float wid)
{
        width = wid;
}

/* function to find the perimeter of the rectangle */
float Rectangle :: perimeter()
{
	rec_perimeter = length + width;
}

/* function to find area of the rectangle */
float Rectangle :: area()
{
	rec_area = length * width;
}

/* function to print the data */
void Rectangle :: show()
{
	cout << "length of rectangle is \t\t" << length << endl;
	cout << "width of rectangle is \t\t" << width << endl;
	cout << "perimeter of rectangle is \t" <<  rec_perimeter << endl;
	cout << "area of rectangle is \t\t" << rec_area << endl;
}

/* functionto check if two rectangles have same area, funtion having arugment as Class object */
int Rectangle :: sameArea(Rectangle recobj1)
{
	if(rec_area == recobj1.rec_area)
	{
		cout << "both rectangles have same area" << endl;
		return 1;
	}
	else
	{
		cout << "both rectangles have different area" << endl;
		return 0;
	}
}

/* functions for the objects */
void Rectangle :: objFunctions()
{
	float num;
	string str;
	float value;
	cout << "Enter the length ";
        cin >> str;
	
	/* validation */
	int valid = validate_float(str, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers";
                exit(0);
        }
        else
        {
                num = value;
        }


	setLength(num); // set length of rectangle
        
	cout << "Enter the width ";
        cin >> str;

	valid = validate_float(str, value );

        if(valid == 1)
        {
                cout << "Enter valid numbers";
                exit(0);
        }
        else
        {
                num = value;
        }

        setWidth(num); // set width of rectanlge
	
	perimeter();
        area();
        show();
}
int main()
{
	Rectangle recobj1, recobj2; // creating 3 objects pf class Rectangle

	recobj1.objFunctions();

	recobj2.objFunctions();

	recobj2.sameArea(recobj1); // check is area of 2nd object is same as 1st one
	
	recobj1.objFunctions();

	recobj2.sameArea(recobj1); // check if area of 2nd object is same as 1st object	
	return 0;
}
