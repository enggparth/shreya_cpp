/**************************************************************************************
 * 
 * Write the definition for a class called complex that has floating point data members
 * for storing real and imaginary parts. The class has the following member functions:
 * void set(float, float) to set the specified value in object
 * void disp() to display complex number object
 * complex sum(complex) to sum two complex numbers & return complex number
 * 1. Write the definitions for each of the above member functions.
 * 2. Write main function to create three complex number objects. Set the value in two
 * objects and call sum() to calculate sum and assign it in third object. Display all
 * complex numbers
 *
 ************************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "6.h"

/*******************
 * NAMESPACE
 *******************/

using namespace std;

/* creating class Complex */
class Complex
{
	public:
		/* keeping real, img and sum as public members so that it can be acessed from outside this scope */
		float real, img, sum;
		void getValues();
		void set(float, float);
		void display();
};

/* function to set data of complex values */

void Complex :: set(float rl, float im)
{
	real = rl;
	img = im;
}

/* to display complex numbers */

void Complex :: display()
{
	cout << "Complex value is " << real << " + i" << img << endl << endl;;
}

/* take data from user and validate it */
void Complex :: getValues()
{
	int valid;
	string str;
	float rl, im, value;
	cout << "Enter real value ";
	cin >> str;
	
        valid = validate_float(str, value);
        
	if(valid == 1)
        {
		cout << "Enter valid input " << endl;
                exit(0);
        }
	else
	{
		rl = value;
	}

	cout << "Enter imaginary number";
        cin >> str;
	
	valid = validate_float(str, value);

        if(valid == 1)
        {
                cout << "Enter valid input " << endl;
                exit(0);
        }
        else
        {
                im = value;
        }
	
	set(rl, im);	
}

int main()
{
	Complex comobj1, comobj2, comobj3; // creating objects of class Complex

	comobj1.getValues(); // setting values of 1st object
	comobj1.display();
	
	comobj2.getValues(); // setting values of 2nd object
	comobj2.display();

	cout << "Summation of both complex numbers is: ";
	comobj3.real = comobj1.real + comobj2.real; // adding the values of both the objects
	comobj3.img = comobj1.img + comobj2.img;
	comobj3.display();
	return 0;
}
