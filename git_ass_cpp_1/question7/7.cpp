/**************************************************************************************
 * 
 * Write the definition for a class called Distance that has data member feet as integer
 * and inches as float. The class has the following member functions:
 * void set(int, float) to give value to object
 * void disp() to display distance in feet and inches
 * 1. Write the definitions for each of the above member functions.
 * 2. Write main function to create three Distance objects. Set the value in two objects
 * and overload ‘+’ operator to calculate sum and assign it in third object. Display all
 * distances. ()
 * 
**************************************************************************************/
/**********************
 * INCLUDE
 *********************/

#include "7.h"

/*******************
 * NAMESPACE 
 *******************/

using namespace std;

/* creating class Distance */
class Distance
{
	public:
		/* keeping feet and inches as public members so that it can be acessed from outside this scope */
		int feet;
	        float inches;
		void set(int , float);
		void display();
		void getValues();
};

/* function to set values */

void Distance :: set(int ft, float in)
{
	feet = ft;
	inches = in;
}

/* taking data from user and validating it */
void Distance :: getValues()
{
        int valid, value_int;
        string str;
        float ft, in, value_ft;
        cout << "Enter feets value ";
        cin >> str;

        valid = validate_int(str, value_int);

        if(valid == 1)
        {
                cout << "Enter valid input " << endl;
                exit(0);
        }
        else
        {
                ft = value_int;
        }

        cout << "Enter inches value ";
        cin >> str;

        valid = validate_float(str, value_ft);

        if(valid == 1)
        {
                cout << "Enter valid input " << endl;
                exit(0);
        }
        else
        {
                in = value_ft;
        }

        set(ft, in);
}


/* function to display values*/

void Distance :: display()
{
	cout << "Distance is "<< feet <<" feet and " << inches << "inches" << endl;
}


int main()
{
	Distance diobj1, diobj2, diobj3;  // creating 3 objcts of Distance class
	diobj1.getValues(); // setting values of 1 object
	diobj1.display();

	diobj2.getValues(); // setting values to 2md object
	diobj2. display();

	cout << "total distance is ";
	diobj3.feet = diobj1.feet + diobj2.feet; // adding the values of both the objects
	diobj3.inches = diobj1.inches + diobj2. inches;

	if(diobj3.inches > 12)
	{
		diobj3.feet++;
		diobj3.inches -= 12;  
	}
	diobj3.display();
	
	return 0;
}
