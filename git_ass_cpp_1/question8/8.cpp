/*****************************************************************************************
 *
 * Write the definition for a class called time that has hours and minutes as integer. The
 * class has the following member functions:
 * void settime(int, int) to set the specified value in object
 * void showtime() to display time object
 * time sum(time) to sum two time object & return time
 * 1. Write the definitions for each of the above member functions.
 * 2. Write main function to create three time objects. Set the value in two objects and
 * call sum() to calculate sum and assign it in third object. Display all time objects.
 *
 ***************************************************************************************/
/**********************
 * INCLUDE
 *********************/
#include "8.h"
/*******************
 * NAMESPACE 
 *******************/
using namespace std;

/* Creating class Time */
class Time
{
		int hours, minutes; // private members
	public:
		/* public member functions */
		void setTime(int, int);
		void showTime();
		void getValues();
		Time sum(Time);
};

/* fuction to set time */
void Time :: setTime(int hrs, int mins)
{
	hours = hrs;
	minutes = mins;
}

/* function to display time */
void Time :: showTime()
{
	cout << "the time is " << hours << " hours and " << minutes << " minutes" << endl;
}

/* sum function with class object return type and class object argumnets */
Time Time :: sum(Time obj1)
{
	Time obj2;
	obj2.hours = hours + obj1.hours;
	obj2.minutes = minutes + obj1.minutes;
	if(obj2.minutes >= 60)
	{
		obj2.hours++;
		obj2.minutes -= 60;
	}
	return obj2;
}

/* take data frmo user and validate it */
void Time :: getValues()
{
        int valid, value;
        string str;
        int hrs, min;
        cout << "Enter hours value ";
        cin >> str;

        valid = validate_int(str, value);

        if(valid == 1)
        {
                cout << "Enter valid input " << endl;
                exit(0);
        }
        else
        {
                hrs = value;
        }

        cout << "Enter minutes value ";
        cin >> str;

        valid = validate_int(str, value);

        if(valid == 1)
        {
                cout << "Enter valid input " << endl;
                exit(0);
        }
        else
        {
                min = value;
        }

        setTime(hrs, min);
}

int main()
{
	Time tiobj1, tiobj2, tiobj3; // creating class objects
	
	tiobj1.getValues(); // settings values for 1st object
	tiobj1.showTime();
	
	tiobj2.getValues(); // setting values for 2nd object
	tiobj2.showTime();
	
	cout << "total time is ";
	tiobj3 = tiobj2.sum(tiobj1); // copy the returned object of sum function in 3rd object
	tiobj3.showTime();
	return 0;
}
