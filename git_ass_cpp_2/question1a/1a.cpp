/*******************************************************************************
 *
 * Write three programs which specify public, private and protected inheritance.
 *
*******************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "1a.h"

/*******************
 * @NAMESPACE 
 *******************/
using namespace std;
/* accessing public, protected and private members*/

class Members
{
	public:
		int num1 = 1;
	protected:
                int num2 = 2;
        private:
                int num3 = 3;
		
	public:
		void accessData() // private data can only be accessed within the class. 
		{
			 cout << "Access public, protected and private data from the derived class num1 = " << num1 << " num2 = " << num2 << " num3 = " << num3 << endl;
			 
		}
		
};

class Derived : Members
{
	public:
		int access_num2 = num2;
		void accessData() // protected data can be accessed within the class as well as from derived class */
		{
			cout << "Access public and protwected data from the derived class num1 = " << num1 << " num2 = " << num2 << endl;
		}
};

void accessData() // public members can be accessed from class, derived class or even outside the class */
{
	Members m ;
	cout << "Access public data outside all class num1 = " << m.num1 << endl;
}

int main()
{
	accessData();

	Members objm;
	Derived objd;

	objd.accessData();
	objm.accessData();

	return 0;
}
