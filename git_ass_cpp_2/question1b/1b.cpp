/*******************************************************************************
 *
 * Write three programs which specify public, private and protected inheritance.
 *
*******************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "1b.h"

/*******************
 * @NAMESPACE 
 *******************/
using namespace std;

/* accessing public functions */
class PublicFun
{
	public:
		int num1 = 1;
	protected:
		int num2 = 2;
	private:
		int num3 = 3;
	public:
		void public_fun()
                {
                        cout << "public function accessing num1 = " << num1 << endl;
		}

		void protected_fun()
                {
                        cout << "protected function accessing num2 = " << num2 << endl;
                }

		void private_fun()
                {
                        cout << "private function accessing num3 = " << num3 << endl;
                }
};

/* create derived class to demonstate access of protected as well as public members of Base class in it */
class Derived : protected PublicFun
{
	public:
	void public_fun()
        {
                 cout << "Derived class accessing public member num1 = " << num1 << endl << endl;
        }

	void protected_fun()
	{
		cout << "Derived class accessing protected member num2 = " << num2 << endl << endl;  
	}
};
int main()
{
	PublicFun pobj;
	Derived dobj;

	cout << "Accessing public member directly " << pobj.num1 << endl;
	pobj.public_fun();
	dobj.public_fun();
	
	cout << "cannot access protected member diectly "<< endl;
	pobj.protected_fun();
	dobj.protected_fun();
	
	cout << "Cannot access private member directly or from derived class" << endl;
	pobj.private_fun();

	return 0;
}
