/*******************************************************************************
 *
 * Write three programs which specify public, private and protected inheritance.
 *
*******************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "1c.h"

/*******************
 * @NAMESPACE 
 *******************/
using namespace std;

/* accessing protected functions */
class ProtectedFun
{
        public:
                int num1 = 1;
        protected:
                int num2 = 2;
        private:
                int num3 = 3;
        protected:
                int num1_fun()
                {
			return num1;
                }

                int num2_fun()
                {
			return num2;
                }

                int num3_fun()
                {
			return num3;
                }
};

/* derived class to access protected functions */
class Derived : protected ProtectedFun
{
		int dnum1 = num1_fun();
		int dnum2 = num2_fun();
		int dnum3 = num3_fun();
	public:		
		void printData()
		{
			 cout << "values from protected functions accessed from derived class are :"<< endl;
			 cout << "num1 = " << dnum1 << endl;
			 cout << "num2 = " << dnum2 << endl;
		         cout << "num3 = " << dnum3 << endl;	
		}
			
};
int main()
{
        ProtectedFun pobj;
        Derived dobj;

        cout << "Accessing public member directly " << pobj.num1 << endl;
	
        cout << "cannot access protected member diectly "<< endl;

        cout << "Cannot access private member directly" << endl;

	dobj.printData();
	
	return 0;
}

