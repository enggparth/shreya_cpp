/*******************************************************************************
 *
 * Write three programs which specify public, private and protected inheritance.
 *
*******************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "2.h"

/*******************
 * @NAMESPACE 
 *******************/

using namespace std;

/*creating a base class */
class Base
{
        public:
		int num1;
	protected:
		int num2;
		Base();
};

Base :: Base()
{
	int value;
	string str;
	cout << "Enter integer number to make it public";
	cin >> str;

	int valid = validate_int(str, value);
	if(valid == 1)
	{
		cout << "Enter a valid integer number " ;
		exit(0);
	}
	else
	{
		num1 = value;
	}

	cout << "Enter integer number  to make it protected";
        cin >> str;

        valid = validate_int(str, value);
        if(valid == 1)
        {
                cout << "Enter a valid integer number " ;
                exit(0);
        }
        else
        {
                num2 = value;
        }
}

/* Dervied1 class with protected access to base class */
class Derived1 : protected Base
{
        public:
                int num4 = num1;
                int num5 = num2;
};

/*
 * Dervied2 class with private access to Derived1 class 
 * we can use num1 and num2 in this class but not in derived classes further as it is accessed private
 */
class Derived2 : private Derived1
{
        public:
                int num7 = num1;
                int num8 = num2;
};

/* 
 * Dervied3 class with private access to Derived2 class 
 * we cannot use num1 and num2 of base class, as base was accessed private in pervious inhertance
 * */
class Derived3 : private Derived2
{
        public:
                int num10 = num7;
                int num11 = num8;
};

int main()
{
	cout << "accessing data from derived1 class which inherit base class with protected access" << endl;
	/* we can use num1 and num2, in derived classes further as it is accessed protected*/
	Derived1 d1;
        cout << d1.num4 << "  " << d1.num5 << endl;
	
	cout << "accessing data from derived2 class which inherit derived1 class with private access" << endl;
	/* we can use num1 and num2 in this class but not in derived classes further as it is accessed private*/
        Derived2 d2;
        cout << d2.num7 << "  " << d2.num8 << endl;

	cout << "accessing data from derived2 class which inherit derived3 class with private access" << endl;
	/* we cannot use num1 and num2 of base class, as base was accessed private in pervious inhertance*/
        Derived3 d3;
        cout << d3.num10 << "  " << d3.num11 << endl;
        return 0;
}
