/*******************************************************************************
 *
 * Write a program that demonstrates a multiple inheritance.
 *
*******************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "3.h"

/*******************
 * @NAMESPACE 
 *******************/

using namespace std;

/* create a base class */
class Base
{
        public:
                int bnum1 = 4;
        protected:
                int bnum2 = 5;
        private:
                int bnum3 = 4;
};

/* derive base class as public access */
class Inherit1 : public Base
{
        public:
        int num1 = bnum1;
        int num2 = bnum2;
};

/* derive base class as protected access */
class Inherit2 : protected Base
{
        public:
        int num1 = bnum1;
        int num2 = bnum2;
};

/* derive base class as private access */
class Inherit3 : private Base
{
        public:
        int num1 = bnum1;
        int num2 = bnum2;
};
int main()
{
        /* we can access bnum1 of Base class with inherit1 becase it is public Derived*/
        Inherit1 i1;
	cout << "Accessing values of public access inherited class" << endl;
        cout << i1.bnum1 << endl << i1.num2 << endl;

        /* we cannot access an members of Base class directly as it is protected Derived , however it is proctected hence can be accessed in further derived classes*/
        Inherit2 i2;
	cout << "Accessing values of protected access inherited class" << endl;
        cout << i2.num1 << endl << i2.num2 << endl;

        /* we cannot access an members of Base class directly as it is private Derived. We can access it in further derived class thougth access in class as members can be accessed within the class */
        Inherit3 i3;
	cout << "Accessing values of private access inherited class" << endl;
        cout << i3.num1 << endl << i3.num2 << endl;

        return 0;
}
