/*******************************************************************************
 *
 * Create a three-level hierarchy of classes with default constructors, along with
 * destructors, both of which announce themselves to cout. Verify that for 
 * an object of the most derived type, all three constructors and destructors are
 * automatically called. Explain the order in which the calls are made.
 *
*******************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "4.h"

/*******************
 * @NAMESPACE 
 *******************/

using namespace std;

/* create base class */
class Base
{
	public:
		Base();
		~Base();
};
/* base class constructors and destructors */
Base :: Base()	
{
	cout << "this is default constructor of base class " << endl;
}
Base :: ~Base()
{
	cout << "this is default destructor of base class " << endl;
}

/* create Inherit1 and inherit Base class with public access */
class Inherit1 : public Base
{
	public:
		Inherit1();
		~Inherit1();
};

/* Inherit1 class constructors and destructors */
Inherit1 :: Inherit1()
{
        cout << "this is default constructor of Inherit1 class " << endl;
}
Inherit1 :: ~Inherit1()
{
        cout << "this is default destructor of Inherit1 class " << endl;
}

/* create class Inherit2 and inherit inherit1 with public access */
class Inherit2 : public Inherit1
{
	public:
	        Inherit2();
	        ~Inherit2();
};

/* Inherit2 class constructors and destructors */
Inherit2 :: Inherit2()
{
        cout << "this is default constructor of Inherit2 class " << endl;
}
Inherit2 :: ~Inherit2()
{
        cout << "this is default destructor of Inherit2 class " << endl;
}

int main()
{
	/* object of most derived type i.e Inherit2 */
	Inherit2 i2obj;
	return 0;
}
