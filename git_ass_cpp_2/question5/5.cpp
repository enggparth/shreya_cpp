 /******************************************************************************************
 *
 * Write a program that defines a shape class with a constructor that gives value to width
 * and height. The define two sub-classes triangle and rectangle, that calculates the area
 * of the shape  area(). In the main, define two variables a triangle and a rectangle and
 * then call the area() function in these two variables.
 *
******************************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "5.h"

/*******************
 * @NAMESPACE 
 *******************/

using namespace std;

/* create class Shape */
class Shape
{
	public:
		float width, heigth;
		Shape();
};

/* define shape constructor to accept value from user and validate it */
Shape :: Shape()
{
	float value;
	string str;
	cout << "Enter the width " << endl;
	cin >> str;

	int valid = validate_float(str, value);
	
	if(valid == 1)
	{
		cout << "Enter a valid number" << endl;
		exit(0);
	}
	else
	{
		width = value;
	}

	cout << "Enter the heigth " << endl;
       	cin >> str;

        valid = validate_float(str, value);

        if(valid == 1)
        {
               cout << "Enter a valid number" << endl;
               exit(0);
        }
        else
        {
              heigth = value;
          }
}

/* create class trianlge */
class Triangle : public Shape
{
	public:
	float area();
};
 /* area() of triangle class */
float Triangle :: area()
{
	float area = (width * heigth) / 2;

	return area;
}


/* create class rectangle */
class Rectangle : public Shape
{
	public:
	float area();
};

/* area() of rectsngle class */
float Rectangle :: area()
{
	float area = width * heigth;
}


int main()
{
	Triangle tobj;
	cout << "area of triangle is " << tobj.area() << endl;
	
	Rectangle robj;
	cout << "area of rectangle is " << robj.area() << endl;
	return 0;
}
