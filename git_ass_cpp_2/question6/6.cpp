/***************************************************************************************
 *
 * Write a program with a mother class and an inherited daughter class. Both of them
 * should have a method  void display() that prints a message (different for mother and
 * daughter). In the main define a daughter and call the  display()  method on it.
 *
***************************************************************************************/
/**********************
 * @INCLUDE
 *********************/

#include "6.h"

/*******************
 * @NAMESPACE 
 *******************/

using namespace std;

/* create class mother */
class Mother
{
	public:
		void display();
};

/* display of mother */
void Mother :: display()
{
	cout << "this is display of mother class " << endl;
}

/* create class daughter */
class Daughter : public Mother
{
	public:
		void display();
};

/* display of daughter */
void Daughter :: display()
{
	cout << "this is display of daugther class" << endl;
}

int main()
{
	Daughter dobj; // create daugther object
	dobj.display(); // invoke daughter display 

	Mother* dobjptr = new Daughter; // mother pointer pointing to daughter class
	dobjptr -> display(); // mother display invoked
	return 0;
}
