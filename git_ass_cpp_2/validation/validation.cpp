#include "validation.h"
using namespace std;
int validate_float(const std::string& str, float& num)
{
	int len = str.length();
	int valid = 1;
	for(int loop = 0; loop < len; loop++)
	{
		if(isdigit(str[loop]) == 0 && str[loop] != '.')
		{
			valid = 0;
			return 1;
		}
		num = stof(str);
	}
	return 0;
}

int validate_int(const std::string& str, int& num)
{
        int len = str.length();
        int valid = 1;
        for(int loop = 0; loop < len; loop++)
        {
                if(isdigit(str[loop]) == 0)
                {
                        valid = 0;
                        return 1;
                }
                num = stoi(str);
        }
        return 0;
}
